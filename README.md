# Güvenli ve Özgür

### Hakkımızda

Güvenli ve Özgür, internette veri gizliliğini sağlamanın basit ve uygulanabilir yöntemlerini tüm internet kullanıcılara ulaştırmayı hedefliyor. İnterneti özgürleştirmenin ilk adımının veri gizliliği konusunda bilinçlenme olduğunu düşünüyor.

Güvenli ve Özgür, veri gizliliğinin ancak özgür araçlar kullanılarak sağlanabileceğinin farkında olarak özgür yazılımı savunuyor.

Güvenli ve Özgür, veri gizliliğini sağlamanın imkansız olduğu anlayışını yayan yaklaşımlarla mücadele ediyor.

### Bağış Yap

Güvenli ve Özgür blogunu kazanç kaygısı gütmeden, toplumsal fayda için yayınlıyoruz. GitLab üzerinde barındırdığımız Güvenli ve Özgür'ün, ufak bir alan adı ücreti dışında bir masrafı yok. Bu haliyle blogu sürdürmekte bir sıkıntı yaşayacağımızı düşünmüyoruz.

Daha fazla insana ulaşmamızı sağlayacak başka araçlar geliştirmemiz için ise daha fazla emek vermemiz dolayısıyla daha fazla zaman ayırmamız gerekiyor. İlk hedeflerimiz arasında, podcast ve video içerik üretimi var.

Bu nedenle desteğinize ihtiyacımız var. Kreosus ve Liberapay üzerinden bize destek olabilirsiniz.<br>
<a href="https://kreosus.com/guvenliveozgur">kreosus.com/guvenliveozgur</a><br>
<a href="https://liberapay.com/guvenliveozgur">liberapay.com/guvenliveozgur</a>

### İletişim

Bizimle mail adresimiz üzerinden iletişim kurabilirsiniz:
<a href="mailto:guvenliveozgur@protonmail.com">guvenliveozgur@protonmail.com</a>
