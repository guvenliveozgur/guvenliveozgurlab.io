---
layout: post
title:  "Google'dan Kurtulmak: Neden ve Nasıl?"
author: eren
categories: [blog]
image: assets/images/google-is-watching-you.jpg
tags: ["google", "android", "gizlilik"]
---

İnternette verilerimizin gizliliğini korumak için öncelikle verilerimizin hangi amaçlarla kimler tarafından topandığını anlamamız gerekiyor. Bize sunulan ücretsiz hizmetlerin nasıl ücretsiz sunulabildiği, ücretli hizmetlerin bizden aldığı tek şeyin paramız olup olmadığı sorularını sormak önemli. Bu amaçla Güvenli ve Özgür üzerinde, teknoloji devi olarak bilinen şirketlerin gelir kaynaklarını daha önce incelemeye çalışmıştık.

"Büyük İnternet Şirketleri Nasıl Para Kazanıyor" başlıklı çevirimizde, Google'ın gelirinin %85'inin dijital reklamlardan elde  ettiği bilgisine yer veriliyordu.[^1] Yine aynı yazıda paylaşıldığı haliyle, Google toplam gelirinin %70'ini arama motoru ve Youtube gibi kendi servisleri üzerindeki reklamlardan, yaklaşık %15'ini ise diğer sitelere reklam vermeye yarayan AdSense ve Admob'dan elde ediyor.

Ana gelir kaynağına göre değerlendirildiğinde Google kocaman bir reklam şirketi. Bize sunduğu 'ücretsiz' hizmetler de aslında bu dijital reklam sisteminin bizim verilerimiz üzerine kurulmuş olmasının bir sonucu. Ne kadar çok kullanıcı o kadar çok veri, ne kadar çok veri o kadar değerli reklam alanları demek. Google servislerinin kullanıcı sayısı ve bu servislerle paylaşılan verilerin çokluğu, 136,8 milyar dolarlık yıllık geliri anlamlı kılıyor.

## Akıllı Telefonlar ve Tablet Bilgisayarlar

Akıllı cihazların %72,6'sı[^2] Android işletim sistemiyle çalışıyor. Bu, şirketlerin kendi dijital ürün ve hizmetlerini Android ile uyumlu hale getirmelerini zorunlu hale getirdiği ve kullanıcıların tek uygulama mağazası üzerinden bütün bu servislere ulaşabilmesini sağlaması açısından değerlendirildiğinde olumlu karşılanabilir. Ancak işin iç yüzü öyle değil.

Android cihazlar, kurulum sırasında cihaza bir Google hesabı tanımlamayı zorunlu kılıyor. Bu hesapla cihazdaki tüm Google servislerini kullanabiliyorsunuz. Sorun buradan itibaren başlıyor.

Google, Play Store üzerinden yaptığınız aramaları, hangi uygulamayı telefonunuza yüklediğinizi, hangi uygulamayı ne zaman ne süreyle kullandığınızı takip ediyor. Kısacası tüm kullanım alışkanlıklarınız kaydediliyor.

Bu verilerle birlikte artık tüm Google hizmetleri size özel yönlendirici içerikler sunmaya başlıyor. Dil öğrenmeye yardımcı bir uygulama indirdiğinizi ve kullandığınızı düşünün. Girdiğiniz her sitede, Google ile bağlantılı her serviste benzer uygulamaları, online dil derslerinin reklamını görmeniz kaçınılmaz olacaktır. Bunun ne kadar önemli olduğu konusunda emin değil misiniz? O zaman şöyle bir örnek düşünelim; bir siyasi partiye ideolojik yakınlığıyla tanınan bir yayın organının Android uygulamasını yüklediniz ve düzenli olarak kullanıyorsunuz. Artık Google sizin siyasi eğiliminiz hakkında bir fikre sahip ve bundan sonra arama motoru dahil tüm servislerde sizin önünüze sunacağı içerikler bu veri hesaba katılarak yapılacak. Yani artık manipüle edilebilir bir durumdasınız.

## Chrome, Arama Motoru ve Diğer İnternet Servisleri

Akıllı telefonlar ve tablet bilgisayarlar başlığı altında çizdiğimiz model, diğer Google servisleri için de benzer şekilde işliyor. Bir Google servisini kullanıyorsunuz, bazı verilerinizi Google ile paylaşıyorsunuz ve tüm Google servisleri sizi manipüle etmenin aracı haline geliyor.

Chrome'da yaptığınız internet gezintileriyle ziyaret ettiğiniz tüm sayfaları Google ile paylaşmış oluyorsunuz. Arama motoru, yaptığınız aramaları ve arama sonucunda hangi siteye girdiğinizi kaydediyor. Haritalar uygulaması, yaptığınız tüm konum aramalarını ve konum geçmişinizi saklıyor. Youtube izlediğiniz tüm videoları sizinle birlikte izliyor. Bütün bu veriler sizin hakkınızda o kadar çok şey anlatıyor ki artık reklamcılar için kolay lokma haline geliyorsunuz.

## Bütün İnternet

Bu bölüme atacağım başlığı bulmakta biraz zorlandım ama Google'ın istatistik ve reklam servisini kullanan site oranı dikkate alınınca böyle bir başlık atmanın sorun olmayacağını düşündüm.

Google, istatistik ve reklam servislerini kullanan sitelere izleyiciler(tracker) yerleştiriyor. Bu izleyiciler sizin Google ile hiçbir bağınız yokken bile Google'ın sizin gezdiğiniz internet sitelerini takip etmesini sağlıyor. Başka bir tarayıcıyla, başka bir arama motoru kullanarak yaptığınız aramalar sonucunda bulduğunuz ürünü satın alırken Google'ın bundan haberdar olması... Biraz korkunç değil mi?

## Google Verileriniz ve Google'a Verdiğiniz İzinler

Google'ın sizin hakkınızda tuttuğu verilerin ve Google'a verdiğiniz izinlerin bir kısmını kullanıcı paneliniz üzerinden görünteleyip yönetebiliyorsunuz. Bu verileri biraz kurcaladığınızda yaptığınız sesli aramaların, ses kayıtlarının bile tutulduğunu görebilirsiniz.

Verilerinizi görüntülemek için [https://myactivity.google.com/myactivity](https://myactivity.google.com/myactivity) adresine, Google'a verdiğiniz izinleri değiştirmek için [https://myaccount.google.com/privacycheckup](https://myaccount.google.com/privacycheckup) adresine gidebilirsiniz.

## Google Kullanmamak

Kendinizi Google'dan korumanın en etkili yolu Google hizmetlerini kullanmamak ve Google'ın sitelere koyduğu izleyicilere karşı önlem almak. Bu dünyanın en zor işi değil. Bu yazıda yazının genelinde geliştirdiğimiz yaklaşımla uyumlu olan birkaç örnek vereceğiz. Bunun bir rehber olma amacı gütmediğini de belirtmek gerek.

**Arama Motoru:** Gizliğinize önem veren, yaptığınız aramaları kaydetmeyen bir arama motoruna geçiş yapmanızı öneriyoruz. Kullandığınız tüm tarayıcılarda da varsayılan arama motoru olarak bu arama motorunu seçmeniz gerekiyor. [DuckDuckGo](https://duckduckgo.com) bu konuda bir alternatif olabilir.

**Tarayıcı:** Chrome, gizli modda gezintilerinizin bile gizli olmadığı bir tarayıcı. Yerine başka bir tarayıcı seçmenizi öneriyoruz. [Firefox](https://www.mozilla.org/en-US/firefox/new/) kararlılığı, eklenti havuzunun genişliği, açık kaynaklı bir proje olması ve gizliliğinize önem vermesiyle öne çıkıyor. Kullanıcıların gizliliğini sağlamasını kolaylaştıran çeşitli özelliklerle birlikte geliyor. Ayrıca mobil sürümüyle de senkronize bir şekilde çalışabiliyor. EFF'in [Privacy Badger](https://addons.cdn.mozilla.net/user-media/previews/full/225/225184.png?modified=1569261286) eklentisini de kurarak daha güvenli bir internet deneyimi yaşabilirsiniz.

**E-posta:** E-postlarınızın bir Google servisinde barınmasını istemezsiniz. Yine açık kaynaklı ve gizliliğinize önem veren bir servisi tercih edebilirsiniz. ProtonMail tatmin edici bir hizmet sunuyor.

**Harita Servisi:** Bulunduğunuz ya da aradığınız lokasyonların Google tarafından bilinmemesini sağlamak için açık kaynaklı [OpenStreetMap](https://www.openstreetmap.org/) altyapısını kullanan uygulamalardan birini kullanabilirsiniz. [OsmAnd](https://osmand.net/) başarılı bir alternatif.

**İşletim Sistemi:** İşletim sistemi söz konusu olduğunda telefonlarda çok fazla alternatifiniz yok. Google servislerinden arındırılmış Android projeleri ve Linux çekirdeği kullanan farklı işletim sistemleri var bunları cihazınıza kurduğunuzda uyumsuzluk problemleri yaşamanız muhtemel. Özellikle Avrupa ve Amerika'da bu işletim sistemlerine sahip telefonların satışını bulabiliyor olsanız da Türkiye'den bu cihazlara erişiminiz yine zor.

Bu konuda biraz araştırma yapmanız yapmanız ve uğraşmanız gerekiyor. Biz de önümüzdeki günlerde telefonunuzu Google servislerinden arındırmanıza yardımcı olacak bir rehber hazırlayıp sizle paylaşacağız.

[^1]: <https://www.guvenliveozgur.net/buyuk-internet-sirketleri-nasil-para-kazaniyor/>
[^2]: <https://www.statista.com/statistics/272698/global-market-share-held-by-mobile-operating-systems-since-2009/>
