---
layout: post
title:  "Büyük İnternet Şirketleri Nasıl Para Kazanıyor?"
author: ulas
categories: [çeviri]
image: assets/images/big-tech-companies.png
tags: ["şirketler", "gelir"]
---
Sekiz şirket internetin tamamında muazzam bir güce sahip: Google, Facebook, Microsoft, Amazon, Apple, Baidu, Alibaba ve Tencent. Birçok internet kullanıcısı bunlardan en az biriyle her gün temas ediyor. Her birinin o kadar çok farklı ürünü, hizmeti ve yatırımı var ki, gelirlerinin ana kaynağının ne olduğu ya da "ücretsiz" olarak sunduğu arama, e-posta, oyun, sosyal medya ve anlık mesajlaşma gibi hizmetlerden nasıl para kazandığı konusu çok net değil.

İnternet devleri nasıl para kazanıyor? Onları birincil gelir kaynaklarına göre dört kategoriye ayırdık.

## Dikkat Tüccarları: Google, Facebook ve Baidu

Dikkatinizi reklamverenlere satarak para kazanmak. Google, Facebook ve Baidu'nun ana işi, çevrimiçiyken yaptığınız herhangi şeylerle ilgili veri toplayıp pazarlamacıların kişiselleştirilmiş reklamlarla sizi bulmasını sağlamaktır. <a href="https://internethealthreport.org/2018/too-big-tech/">Google ve Facebook 2018 yılında Çin'in dışında kalan küresel dijital reklam pazarını kontrol etti.</a>

![Alphabet (Google)](/assets/images/google.png)

### Alphabet (Google)

Google'ın ana şirketi Alphabet gelirlerinin %85'ini dijital reklamlardan elde etmekte. Yaklaşık %70'i Google'ın kendi ürünlerindeki reklamlardan, örneğin Google Arama ve YouTube. Alphabet ayrıca gelirlerinin %14.6'sına tekabül eden -diğer sitelere reklam koymaya yarayan- Google AdSense ve AdMob'a da sahip. Telefon, ev asistanı gibi cihaz satışı ve Play Store'daki uygulamaların gelirleri de Alphabet'in gelirinin %14.5'ini oluşturuyor.

**Gelir:** 136.8 Milyar $[^1]<br>
**Piyasa Değeri:** 795.3 Milyar $[^2]

![Facebook](/assets/images/facebook.png)

### Facebook

Çoğumuz Facebook'un bir "sosyal ağ" şirketi olduğunu düşünüyoruz, ama aslında bir reklam şirketi. Yaklaşık 2.32 milyar aktif kullanıcısıyla kazancının %98.5'inden fazlasını - 55 Milyar $'dan fazla- çoğunlukla Facebook uygulaması üzerinden zaman tünelimizde görünen reklamları satarak sağlıyor. Gelirlerinin kalan %1.5'lik kısmı ise Facebook'taki oyunlardan ve satılan uygulama ve ürünlerden oluşuyor.

**Gelir:** 55.8 Milyar $[^3]<br>
**Piyasa Değeri:** 463.1 Milyar $[^4]

![Baidu](/assets/images/baidu.png)

### Baidu

Baidu <a href="https://gs.statcounter.com/search-engine-market-share/all/china/#monthly-201803-201903">pazar payının %70'iyle</a> Çin'deki en iyi arama motoruna sahip. Geliri ve ayakizi Google'dan daha az, ancak iş modelleri benzer. Baidu gelirinin %80'ini reklam satarak kazanıyor. Geriye kalan %20'lik kısmı ise iQIYI (Netflix benzeri bir yayın servisi) abonelik ücretleri ve ödeme hizmetlerinden sağlıyor. Alphabet gibi Baidu da <a href="https://www.forbes.com/sites/bernardmarr/2018/07/06/how-chinese-internet-giant-baidu-uses-artificial-intelligence-and-machine-learning/#774c2d912d55">yapay zeka</a> ve otonom araçlar gibi araştırmalara yatırım yapıyor.

**Gelir:** 14.9 Milyar $[^5]<br>
**Piyasa Değeri:** 56.6 Milyar $[^6]

## Makinistler: Apple ve Microsoft

Microsoft ve Apple gelirlerinin çoğunu çevrimiçi dünyaya erişmemizi sağlayan cihaz ve yazılımları üretip satarak elde ediyor. Cep telefonları, bilgisayarlar, oyun konsolları -ya da ofis yazılımı ve bulut depolama hizmetleri- gelir elde etmelerini sağlayan ürünlerden bazıları.

![Microsoft](/assets/images/microsoft.png)

### Microsoft

Microsoft'un gelirlerinin %70.8'ini ürün satışları oluşturuyor. Ancak bu ürünler birçok kategoriye ayrılabilir, mesela Microsoft Office gibi "verimlilik" yazılımları ve çevrimiçi işe alım platformu LinkedIn. Ayrıca ürün satışı kategorilerine Windows gibi yazılımlar, Xbox ve Surface gibi donanımlar ve arama reklamcılığı dahil. Microsoft'un bulut tabanlı hizmetleri ise 2018'de toplam gelirlerinin %29.2'sini oluşturdu.

**Gelir:** 110.4 Milyar $[^7]<br>
**Piyasa Değeri:** 863.4 Milyar $[^8]

![Apple](/assets/images/apple.png)

### Apple

Apple gelirinin %86'sını dijital cihaz ve bilgisayar satışlarından elde ediyor. iPhone satışları gelirlerin büyük bir kısmını oluşturmakta. Apple'ın 2018 gelirinin yarısından fazlasını -yaklaşık 167 milyar $- iPhone satışlarıyla elde etti. Mac satışları ürün satışlarının %9.6'sına, iPad satışları ise %7'sine ait. iCloud, Apple Care ve Apple Pay gibi hizmetler şirket gelirlerinin %14'ünü oluşturuyor.

**Gelir:** 265.6 Milyar $[^9]<br>
**Piyasa Değeri:** 825 Milyar $[^10]

## Perakende Aracıları: Amazon ve Alibaba

Amazon ve Alibaba'nın öncelikli para kazanma yöntemi bize internetten bir şeyler satmak. Ayrıca hem Amazon hem de Alibaba, çevrimiçi deneyimlere entegre fiziksel perakende mağazalar açmaya başladı. Fazlası da var. İkisi de çevrimiçi yayın servisi, lojistik hizmeti, bulut hizmetleri, para transferi hizmeti ve yemek dağıtımı hizmeti veriyor. Dijital reklam da satıyorlar.

![Amazon](/assets/images/amazon.png)

### Amazon

Bir zamanlar kitap satıyordu, şu an her şeyi satıyor. Amazon gelirlerinin %78.5'ini perakende satıştan elde ediyor. Amazon Prime abonelik ücretleri gelirlerinin %6'sını oluşturmakta. Bilgisayar gücü, veritabanı depolama ve barındırma gibi hizmetler sunan Amazon Web Services ise 2018 gelirlerinin %11'ini oluşturdu.

**Gelir:** 232.9 Milyar $[^11]<br>
**Piyasa Değeri:** 821.2 Milyar $[^12]

![Alibaba](/assets/images/alibaba.png)

### Alibaba

Alibaba, gelirlerinin %85.6'sını Çin'deki 552 milyon müşterisine mal satarak, geri kalanını ise dijital reklamlardan, Youku Todou (bir video akış hizmeti) abonelik ücretlerinden ve bulut tabanlı servislerden kazanıyor. Alibaba bulut hizmetleri sunuyor, farklı işlerini entegre etmeye ve dijitalleştirmeye yatırımlar yapıyor. Ayrıca Alibaba günlük 60 milyon kullanıcısı olan AutoNavi adında bir harita yazılımı geliştirmeye devam ediyor.

**Gelir:** 39.9 Milyar $[^13]<br>
**Piyasa Değeri:** 476.7 Milyar $[^14]

## Çok Yönlü Şirket: Tencent

Tencent özellikle mesajlaşma platformu WeChat ile tanınıyor, ancak şirket doğrudan ondan ne kadar para kazandığını açıklamıyor. WhatsApp veya Telegram'ın aksine, WeChat bir measajlaşma uygulamasından çok daha fazlası. Çin'deki günlük hayata derinlemesine entegre edilmiştir ve fatura ödeme gibi şeyler yapmanıza ve bir doktor randevusu almanızı sağlayabilir.

![Tencent](/assets/images/tencent.png)

### Tencent

Tencent'ın gelirinin çoğunluğu, uygulama içi satın alımlarından (çoğunlukla oyunlardaki satın alımlar) ve Tencent Video'daki (2018 gelirinin %56,6'sı) abonelik ücretlerinden gelir. Tencent için giderek daha önemli hale gelen bir bölüm, çoğunlukla WeChat Pay üzerinden çevrimiçi işlem ücretleri anlamına gelen ödeme hizmetleridir. Bu kısım 2018 gelirinin %24.9'unu oluşturmakta. Tencent bunun dışında medya platformlarında ve mesajlaşma uygulamasında gösterdiği dijital reklamlarla para kazanıyor.

**Gelir:** 45.5 Milyar $[^15]<br>
**Piyasa Değeri:** 397.2 Milyar $[^16]

**Yazının orijinali:** <https://internethealthreport.org/2019/how-the-biggest-internet-companies-make-money/>

<!--- Footnotes -->

[^1]: <https://www.sec.gov/Archives/edgar/data/1652044/000165204419000004/goog10-kq42018.htm>
[^2]: <https://finance.yahoo.com/quote/GOOG?p=GOOG&.tsrc=fin-srch>
[^3]: <https://investor.fb.com/financials/sec-filings-details/default.aspx?FilingId=13183451>
[^4]: <https://finance.yahoo.com/quote/FB?p=FB&.tsrc=fin-srch>
[^5]: <http://media.corporate-ir.net/media_files/IROL/18/188488/2019/Baidu%202018%20Form%2020-F.pdf>
[^6]: <https://finance.yahoo.com/quote/BIDU?p=BIDU&.tsrc=fin-srch>
[^7]: <https://www.microsoft.com/en-us/annualreports/ar2018/annualreport>
[^8]: <https://finance.yahoo.com/quote/MSFT?p=MSFT&.tsrc=fin-srch>
[^9]: <https://s2.q4cdn.com/470004039/files/doc_financials/2018/q4/10K_Q4FY18.pdf>
[^10]: <https://finance.yahoo.com/quote/AAPL?p=AAPL&.tsrc=fin-srch>
[^11]: <https://s2.q4cdn.com/299287126/files/doc_financials/annual/2018-Annual-Report.pdf>
[^12]: <https://finance.yahoo.com/quote/AMZN?p=AMZN&.tsrc=fin-srch>
[^13]: <https://www.alibabagroup.com/en/news/press_pdf/p180504.pdf>
[^14]: <https://finance.yahoo.com/quote/BABA?p=BABA&.tsrc=fin-srch>
[^15]: <http://cdc-tencent-com-1258344706.image.myqcloud.com/storage/uploads/2019/11/09/da62661e976ea6cf64551dc5cdf079ea.pdf>
[^16]: <https://finance.yahoo.com/quote/TCEHY?p=TCEHY&.tsrc=fin-srch>
