---
layout: post
title:  "Özgür ve Güvenli Bir İnternet Mümkün"
author: eren
categories: [blog]
image: assets/images/guvenli-ve-ozgur-internet-mumkun.jpg
tags: ["veri gizliliği", "güvenli internet"]
---
İnternet kullanımı yaygınlaştıkça ve kullandığımız teknolojik araçlar hayatımızın her alanında kendilerine daha fazla yer buldukça veri gizliliği sorunu daha sık gündeme gelmeye başladı. İnsanlar, internete bağlı oldukları her an bilerek ya da bilmeyerek pek çok şirketle ve doğrudan ya da dolaylı olarak devletlerle verilerini paylaşıyorlar. Bu durum, bunun farkında olan insanlar tarafından çok büyük bir sorun olarak görülse bile büyük ölçüde ilgisiz ve dikkatsiz davranılarak önlem alınmadığı ortada. Bir de farkında olmayanlar var tabii.

İnsanlar, gündelik yaşantıda teknolojik araçları kullanım amaçları ve alışkanlıkları gereği gizlilik sorununu çoğu zaman bir kullanıcının verilerine başka hangi kullanıcıların erişip erişemediği üzerinden değerlendiriyor. Gizlilik sorununun bunun ötesinde bir sorun olduğu, şirketlerle ve devletlerle paylaşılan verilerin de gerçek gizlilik ve özgür internet deneyimi için sorgulanması gerektiği atlanıyor.

Gizlilik sorununu dillendiren ve gündem haline getiren pek çok kişi de var. Ancak bu kişilerin birçoğu tarafından, insanları “her yerde gözü olan teknoloji şirketleri” ve “karşı konulamaz devlet gücü” karşısında çaresizliğe iten ve dolayısıyla önlem almalarını değil, sorunu görmezden gelerek yaşamaya devam etmelerine yol açacak şekilde paylaşımlar yapıldığını görüyoruz. Bu paylaşımları yapanların bir kısmı bunu bir cehaletin sonucu olarak yapıyor, azımsanmayacak bir kısmıysa gizlilik sorununa dair genel bilgisizliği kişisel fayda sağlamak için kullanmaya çalışıyor.

Toplumun büyük bir bölümü veri gizliliği sorununu ıskalıyor, bir şekilde bu sorun üzerine tartışmalara dahil olanlar ise yanlış bilgilerle çözümden uzak tutuluyor. Güvenli ve Özgür'ün açılması da büyük ölçüde bu duruma müdahale etme amacı taşıyor.

Veri gizliliğinin herkes tarafından uygulanabilecek olan yöntemlerle sağlanabileceğini biliyoruz.Bilgi birikimimiz doğrultusunda özgün üretimlerle ve takip ettiğimiz kaynakların çevirilmesiyle güvenli ve özgür bir internet deneyimi için alınması gereken basit önlemleri ve dikkat edilmesi gerekenleri sade bir dille insanlara anlatma amacıyla bu bloga başlıyoruz.
