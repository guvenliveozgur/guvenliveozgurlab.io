---
layout: page
title: Hakkımızda
permalink: /hakkimizda
---

<div class="row justify-content-between">
<div class="col-md-8 pr-5">

<p><b>Güvenli ve Özgür</b>, veri gizliliğini sağlamanın basit ve uygulanabilir yöntemlerini tüm internet kullanıcılarına ulaştırmayı hedefliyor. İnterneti özgürleştirmenin ilk adımının veri gizliliği konusunda bilinçlenme olduğunu düşünüyor.</p>

<p><b>Güvenli ve Özgür</b>, veri gizliliğinin ancak özgür araçlar kullanılarak sağlanabileceğinin farkında olarak özgür yazılımı savunuyor.</p>

<p><b>Güvenli ve Özgür</b>, veri gizliliğini sağlamanın imkansız olduğu anlayışını yayan komplocu yaklaşımlarla mücadele ediyor.</p>

<hr>
<br>

<h5>İletişim</h5>

<p>Bizimle mail adresimiz üzerinden iletişim kurabilirsiniz:</p>

<p><a href="mailto:guvenliveozgur@protonmail.com">guvenliveozgur@protonmail.com</a></p>

</div>

<div class="col-md-4">

<div class="sticky-top sticky-top-80">
<h5>Bize destek olun</h5>

<p>Güvenli ve Özgür'ü, kazanç kaygısı gütmeden, toplumsal fayda için yayınlıyoruz. GitLab üzerinde barındırdığımız Güvenli ve Özgür'ün, ufak bir alan adı ücreti dışında bir masrafı yok. Bu haliyle blogu sürdürmekte bir sıkıntı yaşayacağımızı düşünmüyoruz.</p>

<p>Daha fazla insana ulaşmamızı sağlayacak başka araçlar geliştirmemiz için ise daha fazla emek vermemiz dolayısıyla daha fazla zaman ayırmamız gerekiyor. İlk hedeflerimiz arasında, podcast ve video içerik üretimi var.</p>

<p>Bu nedenle biraz destek olsanız fena olmaz. Kreosus ve Liberapay üzerinden bize destek olabilirsiniz.</p>

<p><a target="_blank" href="https://kreosus.com/guvenliveozgur">kreosus.com/guvenliveozgur</a></p>
<p><a target="_blank" href="https://liberapay.com/guvenliveozgur">liberapay.com/guvenliveozgur</a></p>




</div>
</div>
</div>
